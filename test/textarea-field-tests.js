describe('Text Area Field Test', function() {
    var $compile,
    $rootScope;

    beforeEach(module('idonate.core.inputs'));

    beforeEach(inject(function(_$compile_, _$rootScope_){
        $compile = _$compile_;
        $rootScope = _$rootScope_;
    }));

    it('displays', function() {
        // Compile a piece of HTML containing the directive
        var element = $compile("<textarea-field></textarea-field>")($rootScope);
        // fire all the watches, so the scope expression {{1 + 1}} will be evaluated
        $rootScope.$digest();
        // Check that the compiled element contains the templated content
        expect(true).toBe(true);
    }) 
});