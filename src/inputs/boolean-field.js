var uniqueId = 1;

angular.module('idonate.core.inputs')
.directive('booleanField', BooleanField);

function BooleanField() {
  return {
    bindToController: {
      blur: '=?',
      disable: '=?',
      field: '=?',
      label: '@?',
      off: '@?',
      on: '@?',
      required: '=?',
      type: '@?',
      valid: '=?'
    },
    controller: BooleanFieldCtrl,
    controllerAs: 'ctrl',
    restrict: 'E',
    scope: {},
    templateUrl: 'boolean-field.html'
  };

  /*@ngInject*/
  function BooleanFieldCtrl($scope,$timeout) {
    var ctrl = this;

    init();

    $scope.$on('validate', validate);

    function init() {
      ctrl.uniqueId = 'booleanField' + uniqueId++
      if(ctrl.on == undefined) {
        ctrl.on = 'On';
      }
      if(ctrl.off == undefined) {
        ctrl.off = 'Off';
      }
      if(!ctrl.type) {
        ctrl.type = 'switch';
      }
    }

    function changed() {
      if(ctrl.blur) {
        $timeout(function() {
          ctrl.blur()
        });
      }
    }

    function validate() {
      if(ctrl.required && !ctrl.field) {
        ctrl.valid = false;
      } else {
        ctrl.valid = true;
      }
    }
  }
}