var uniqueId = 1;

angular.module('idonate.core.inputs')
.directive('imageField', ImageField);

function ImageField() {
  return {
    bindToController: {
      description: '=?',
      disable: '=?',
      field: '=',
      label: '@?',
      url: '=',
      required: '=?',
      session: '=',
      type: '@',
      valid: '=?'
    },
    controller: ImageFieldCtrl,
    controllerAs: 'ctrl',
    restrict: 'E',
    scope: {},
    templateUrl: 'image-field.html'
  };

  /*@ngInject*/
  function ImageFieldCtrl($scope, $timeout, Upload) {
    var ctrl = this;
    var file = null;
    var file_data = {};

    ctrl.upload = uploadPhoto;
    ctrl.remove = remove;

    init();

    $scope.$on('validate', validate);

    function init() {
      $timeout(function() {
        if(ctrl.field && ctrl.field.value) { 
          ctrl.uploaderUrlImage = ctrl.field.value;
        }
        file_data = null;
      });

      $scope.$watch('ctrl.files', function(files) {
        files && uploadPhoto(files);
      });

      $scope.$watch('ctrl.field', function(nv, ov) {
        if(nv) {
          ctrl.field.name = nv.name;
          ctrl.uploaderUrlImage = nv.value;
        }
      });

      $scope.$watch('ctrl.field.value', function(newVal) {
        ctrl.uploaderUrlImage = newVal;
      });
    }

    function remove() {
      console.log(ctrl)
      if(!ctrl.disable) {
        ctrl.field.value = "";
        ctrl.uploaderUrlImage = null;
        ctrl.files = null
      }
    }

    function uploadPhoto(files) {
      if(files && files.name) {
        file = files;
        file_data = {};
        file_data[ctrl.field.name] = file;
        Upload.upload({
          url: ctrl.field.url,
          data: file_data,
          headers: {
            'x-session-key': ctrl.session
          }
        })
        .progress(function(evt) {
          ctrl.uploadProgress = parseInt(100.0 * evt.loaded / evt.total);
        })
        .success(function(data, status, headers, config) {
          ctrl.uploadProgress = 0;
          file = null;
          file_data = {};
          ctrl.field.value = data.result;
          ctrl.uploaderUrlImage = data.result;
          validate();
        });
      }
    }

    function validate() {
      if(!ctrl.uploaderUrlImage && ctrl.required) {
        ctrl.valid = false;
      } else {
        ctrl.valid = true;
      }
    }
  }
}
