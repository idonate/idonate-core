var uniqueId = 1;

angular.module('idonate.core.inputs')
.directive('textField', TextField);

function TextField() {
  return {
    bindToController: {
      blur: '=?',
      disable: '=?',
      field: '=?',
      label: '@?',
      mask: '@?',
      required: '=?',
      type: '@',
      valid: '=?',
      validator: '=?',
      placeholder: '=?',
      passwordMatch: '=?',
      passwordStrength:'=?',
      code: '=?',
    },
    compile: TextFieldCompile,
    controller: TextFieldCtrl,
    controllerAs: 'ctrl',
    restrict: 'E',
    scope: {},
    templateUrl: 'text-field.html'
  };

  function TextFieldCompile(element, attrs) {
    if(!attrs.type) {
      attrs.type = 'text';
    }
  }

  function TextFieldCtrl($scope, iso4217) {
    var ctrl = this;
    ctrl.uniqueId = 'textField' + uniqueId++;
    ctrl.onBlur = onBlur;
    ctrl.valid = null;

    $scope.$on('validate', validate);
    $scope.$watch('ctrl.code', function(newVal) {
      if(!newVal) { return ctrl.curCode = '$'; }
      ctrl.curCode = iso4217.getCurrencyByCode(newVal).symbol;
    });

    function onBlur() {
      if(ctrl.required) {
        validate();
      }

      if(ctrl.blur) {
        ctrl.blur();
      }
    }

    function validate() {
      if(!ctrl.field && ctrl.required) {
        ctrl.valid = false;
      }
      else {
        ctrl.valid = true;
      }
      if(ctrl.validator) {
        ctrl.valid = ctrl.validator(ctrl.field, ctrl.valid);
      }
    }
  }
}
