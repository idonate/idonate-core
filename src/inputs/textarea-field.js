var uniqueId = 1;

angular.module('idonate.core.inputs')
.directive('textareaField', TextareaField);

function TextareaField() {
  return {
    bindToController: {
      blur: '=?',
      disable: '=?',
      field: '=?',
      float: '=?',
      label: '@?',
      required: '=?',
      rows: '=?',
      valid: '=?',
      validator: '=?'
    },
    controller: TextareaFieldCtrl,
    controllerAs: 'ctrl',
    restrict: 'E',
    scope: {},
    templateUrl: 'textarea-field.html'
  };

  /*@ngInject*/
  function TextareaFieldCtrl($scope, $timeout) {
    var ctrl = this;
    ctrl.uniqueId = 'textareaField' + uniqueId++;
    ctrl.onBlur = onBlur;

    ctrl.valid = null;

    ctrl.init = function() {
      $timeout(function() {
        if (ctrl.disable){
          $scope.editor.summernote('disable');
        }
      });
    }

    $scope.$watch('ctrl.disable', function(newVal) {
      if(!newVal) {
        $timeout(function() {
          $scope.editor.summernote('enable');
        });
      }
    });

    var defaultOptions = {
      height: 300,
      focus: false,
      toolbar: [['style', ['bold', 'italic', 'underline']]],
      cleaner: {
        action:'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
        newline:'<p><br></p>', // Summernote's default is to use '<p><br></p>'
      },
      iconPrefix: '',
      icons: {
        'align': 'fa icon-align',
        'alignCenter': 'fa fa-align-center',
        'alignJustify': 'fa fa-align-justify',
        'alignLeft': 'fa fa-align-left',
        'alignRight': 'fa fa-align-right',
        'indent': 'fa fa-indent',
        'outdent': 'fa fa-outdent',
        'arrowsAlt': 'fa fa-arrows-alt',
        'bold': 'fa fa-bold',
        'caret': 'fa fa-caret-down',
        'circle': 'fa fa-circle',
        'close': 'fa fa-times',
        'code': 'fa fa-code',
        'eraser': 'fa fa-eraser',
        'font': 'fa fa-font',
        'frame': 'fa icon-frame',
        'italic': 'fa fa-italic',
        'link': 'fa icon-link',
        'unlink': 'fa fa-chain-broken',
        'magic': 'fa fa-magic',
        'menuCheck': 'fa fa-check',
        'minus': 'fa fa-minus',
        'orderedlist': 'fa fa-list-ol',
        'pencil': 'fa fa-pencil',
        'picture': 'fa fa-picture-o',
        'question': 'fa fa-question',
        'redo': 'fa fa-redo',
        'square': 'fa fa-square',
        'strikethrough': 'fa fa-strikethrough',
        'subscript': 'fa fa-subscript',
        'superscript': 'fa fa-superscript',
        'table': 'fa fa-table',
        'textHeight': 'fa fa-text-height',
        'trash': 'fa fa-trash',
        'underline': 'fa fa-underline',
        'undo': 'fa fa-undo',
        'unorderedlist': 'fa fa-ul',
        'video': 'fa fa-video'
      }
    };

    ctrl.options = defaultOptions;

    $scope.$on('validate', validate);

    function onBlur() {
      if(ctrl.required) {
        validate();
      }

      if(ctrl.blur) {
        ctrl.blur();
      }
    }

    function validate() {
      if(!ctrl.field && ctrl.required) {
        ctrl.valid = false;
      }
      else {
        ctrl.valid = true;
      }
      if(ctrl.validator) {
        ctrl.valid = ctrl.validator(ctrl.field, ctrl.valid);
      }
    }
  }
}
