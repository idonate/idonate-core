angular.module('idonate.core.inputs')
.directive('idSubmit', iDSubmit);

function iDSubmit($timeout,$parse) {
  return {
    restrict: 'A',
    require: 'form',
    link: iDSubmitLink
  };

  function iDSubmitLink(scope, element, attrs) {
    scope.valid = {};
    var fn = $parse(attrs.idSubmit);
    element.bind('submit', function(event) {
      $timeout(function() {
        scope.$broadcast('validate');
        $timeout(function() {
          var valid = Object.keys(scope.valid).every(function(k){return this[k]}, scope.valid);
          if(valid) {
            fn(scope, {$event:event});
          }
        });
      });
    })
  }
}
