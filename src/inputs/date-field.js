var uniqueId = 1;

angular.module('idonate.core.inputs')
.component('dateField', register());

function register() {
  return {
    templateUrl: 'date-field.html',
    bindings: {
      required: '=?',
      valid: '=?',
      ngModel: '=',
      placeholder: '@',
      ngDisabled: '<',
      label: '@',
      userOptions: '<options',
      description: '@',
      ngBlur: '&'
    },
    controller: DateField
  };
}

function DateField($timeout, $scope) {
  var priv = {};
  this.uniqueId = 'dateField' + uniqueId++;
  priv.$timeout = $timeout;
  setDefaults.bind(this)();
  $scope.$watch('$ctrl.ngModel', blur.bind(this));
  this.$onChanges = $onChanges.bind(this);
  return this;

  function $onChanges(changesObj) {
    if(changesObj['userOptions']) {
      setDefaults.bind(this)();
    }
  }

  function setDefaults() {
    if(!this.options) {
      this.options = {};
    }
    angular.extend(this.options, {
      format: 'MM-DD-YYYY hh:mm A z',
      startView: 'month',
      today: 'true',
      minView: 'year',
      maxView: 'hour',
      minDate: moment(),
      startDate: moment(this.ngModel)
    }, this.userOptions);
  }

  function blur() {
    priv.$timeout((function() {
      this.ngBlur();
    }).bind(this));
  }
}
