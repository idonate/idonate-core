;(function() {
"use strict";

var dependencies = [
  'ui.select',
  'ngFileUpload',
  'moment-picker',
  'summernote',
  'zxcvbn',
  'isoCurrency',
];

angular.module('idonate.core.inputs', dependencies)
.config(Config);

function Config() {

}
}());

;(function() {
"use strict";

var uniqueId = 1;

angular.module('idonate.core.inputs')
.directive('booleanField', BooleanField);

function BooleanField() {
  BooleanFieldCtrl.$inject = ["$scope", "$timeout"];
  return {
    bindToController: {
      blur: '=?',
      disable: '=?',
      field: '=?',
      label: '@?',
      off: '@?',
      on: '@?',
      required: '=?',
      type: '@?',
      valid: '=?'
    },
    controller: BooleanFieldCtrl,
    controllerAs: 'ctrl',
    restrict: 'E',
    scope: {},
    template:'<div class=\"form-group checkbox-directive\"><label ng-if=ctrl.label>{{ctrl.label}}</label><div ng-switch on=ctrl.type class=checkbox-wrapper><div class=check-box-container ng-switch-when=blocky><label class=checkbox><input ng-disabled=ctrl.disable type=checkbox class=check-box-input ng-checked=\"ctrl.field == true\" ng-model=ctrl.field ng-change=ctrl.changed()> <span>{{ctrl.on}}</span> <span>{{ctrl.off}}</span></label></div><div class=onoffswitch ng-switch-when=switch><input type=checkbox name={{ctrl.uniqueId}} class=onoffswitch-checkbox id={{ctrl.uniqueId}} ng-disabled=ctrl.disable ng-checked=\"ctrl.field == true\" ng-model=ctrl.field ng-change=ctrl.changed()> <label class=onoffswitch-label for={{ctrl.uniqueId}}><span class=onoffswitch-inner></span> <span class=onoffswitch-switch></span></label></div></div></div>'
  };

  /*@ngInject*/
  function BooleanFieldCtrl($scope,$timeout) {
    var ctrl = this;

    init();

    $scope.$on('validate', validate);

    function init() {
      ctrl.uniqueId = 'booleanField' + uniqueId++
      if(ctrl.on == undefined) {
        ctrl.on = 'On';
      }
      if(ctrl.off == undefined) {
        ctrl.off = 'Off';
      }
      if(!ctrl.type) {
        ctrl.type = 'switch';
      }
    }

    function changed() {
      if(ctrl.blur) {
        $timeout(function() {
          ctrl.blur()
        });
      }
    }

    function validate() {
      if(ctrl.required && !ctrl.field) {
        ctrl.valid = false;
      } else {
        ctrl.valid = true;
      }
    }
  }
}
}());

;(function() {
"use strict";

var uniqueId = 1;

angular.module('idonate.core.inputs')
.component('dateField', register());

function register() {
  return {
    template:'<div class=form-group><label for={{::$ctrl.uniqueId}} ng-if=\"$ctrl.label || $ctrl.description\">{{$ctrl.label}} <i ng-if=$ctrl.field.description tooltip={{$ctrl.field.description}} class=icon-question-mark></i></label><div class=input-group moment-picker=$ctrl.displayModel ng-model=$ctrl.ngModel format={{$ctrl.options.format}} start-view={{$ctrl.options.startView}} today=$ctrl.options.today min-view={{$ctrl.options.minView}} max-view={{$ctrl.options.maxView}} min-date=$ctrl.options.minDate max-date=$ctrl.options.maxDate disable=$ctrl.ngDisabled start-date=$ctrl.options.startDate><span class=input-group-addon><i class=icon-calendar></i></span> <input class=\"date-picker form-control\" placeholder=\"{{ $parent.$parent.$ctrl.placeholder }}\" ng-model=$ctrl.displayModel ng-class=\"{\'disabled\': $ctrl.ngDisabled}\" ng-disabled=$ctrl.ngDisabled disable=$ctrl.ngDisabled></div></div>',
    bindings: {
      required: '=?',
      valid: '=?',
      ngModel: '=',
      placeholder: '@',
      ngDisabled: '<',
      label: '@',
      userOptions: '<options',
      description: '@',
      ngBlur: '&'
    },
    controller: DateField
  };
}

function DateField($timeout, $scope) {
  var priv = {};
  this.uniqueId = 'dateField' + uniqueId++;
  priv.$timeout = $timeout;
  setDefaults.bind(this)();
  $scope.$watch('$ctrl.ngModel', blur.bind(this));
  this.$onChanges = $onChanges.bind(this);
  return this;

  function $onChanges(changesObj) {
    if(changesObj['userOptions']) {
      setDefaults.bind(this)();
    }
  }

  function setDefaults() {
    if(!this.options) {
      this.options = {};
    }
    angular.extend(this.options, {
      format: 'MM-DD-YYYY hh:mm A z',
      startView: 'month',
      today: 'true',
      minView: 'year',
      maxView: 'hour',
      minDate: moment(),
      startDate: moment(this.ngModel)
    }, this.userOptions);
  }

  function blur() {
    priv.$timeout((function() {
      this.ngBlur();
    }).bind(this));
  }
}
}());

;(function() {
"use strict";

iDSubmit.$inject = ["$timeout", "$parse"];
angular.module('idonate.core.inputs')
.directive('idSubmit', iDSubmit);

function iDSubmit($timeout,$parse) {
  return {
    restrict: 'A',
    require: 'form',
    link: iDSubmitLink
  };

  function iDSubmitLink(scope, element, attrs) {
    scope.valid = {};
    var fn = $parse(attrs.idSubmit);
    element.bind('submit', function(event) {
      $timeout(function() {
        scope.$broadcast('validate');
        $timeout(function() {
          var valid = Object.keys(scope.valid).every(function(k){return this[k]}, scope.valid);
          if(valid) {
            fn(scope, {$event:event});
          }
        });
      });
    })
  }
}
}());

;(function() {
"use strict";

var uniqueId = 1;

angular.module('idonate.core.inputs')
.directive('imageField', ImageField);

function ImageField() {
  ImageFieldCtrl.$inject = ["$scope", "$timeout", "Upload"];
  return {
    bindToController: {
      description: '=?',
      disable: '=?',
      field: '=',
      label: '@?',
      url: '=',
      required: '=?',
      session: '=',
      type: '@',
      valid: '=?'
    },
    controller: ImageFieldCtrl,
    controllerAs: 'ctrl',
    restrict: 'E',
    scope: {},
    template:'<div class=file-uploader-container ng-switch on=ctrl.type><label>{{ctrl.label}} <i ng-if=ctrl.description tooltip={{ctrl.description}} class=icon-question-mark></i></label><div ng-switch-when=buttons class=\"buttons-uploader row\"><div class=col-7 ng-show=!ctrl.uploaderUrlImage><div class=\"file-drop-area text-center\" ng-class=\"{\'file-present\':uploaderUrlImage || ctrl.files.name}\" ngf-drop ng-model=ctrl.files ngf-drag-over-class=dragover ngf-multiple=false ngf-accept=\"\'.jpg,.png\'\" ng-disabled=ctrl.disable>Drop your image here</div></div><div class=\"image-preview col-md-auto mb-1 mb-md-0 text-center-sm\" ng-show=\"ctrl.uploaderUrlImage || ctrl.files.name\"><div ng-show=\"!ctrl.uploaderUrlImage && ctrl.files.name\"><img class=img-fluid ngf-src=ctrl.files.value></div><div ng-show=\"ctrl.uploaderUrlImage && !ctrl.file\"><img class=img-fluid ng-src={{ctrl.uploaderUrlImage}}></div></div><div class=\"buttons-container col-md-auto\"><a class=\"btn btn-outline-primary d-block mb-1\" ng-show=!ctrl.disable ng-class=\"{\'file-present\':uploaderUrlImage || ctrl.files.name}\" ng-model=ctrl.files ngf-multiple=false ngf-select ngf-accept=\"\'.jpg,.png\'\" tabindex=0>Upload image</a> <a class=\"btn btn-outline-danger d-block\" ng-show=\"!ctrl.disable && (ctrl.uploaderUrlImage || ctrl.files.name)\" ng-click=ctrl.remove() ng-disabled=ctrl.disable tabindex=0>Remove image</a></div><div class=\"col-12 mt-2\" ng-show=\"ctrl.uploadProgress > 0\"><div class=progress><div class=progress-bar style=width:{{ctrl.uploadProgress}}%></div></div></div></div><div ng-switch-when=round class=\"round-uploader row\"><div class=file-drop-area ng-class=\"{\'file-present\':uploaderUrlImage || ctrl.files.name}\" ngf-select ngf-drop ng-model=ctrl.files ngf-drag-over-class=dragover ngf-selectngf-multiple=false ngf-accept=\"\'.jpg,.png\'\" ng-disabled=ctrl.disable><a ng-show=!ctrl.uploaderUrlImage>Upload image</a> <a ng-show=ctrl.uploaderUrlImage>Change image</a><div class=image-preview><div ng-show=\"!ctrl.uploaderUrlImage && ctrl.files.name\"><img class=img-fluid ngf-src=ctrl.files.value></div><div ng-show=\"ctrl.uploaderUrlImage && !ctrl.file\"><img class=img-fluid ng-src={{ctrl.uploaderUrlImage}}></div></div></div><div class=\"col-12 mt-2\" ng-show=\"ctrl.uploadProgress > 0\"><div class=progress style=\"height: 4.5px;\"><div class=progress-bar style=width:{{ctrl.uploadProgress}}%></div></div></div></div></div>'
  };

  /*@ngInject*/
  function ImageFieldCtrl($scope, $timeout, Upload) {
    var ctrl = this;
    var file = null;
    var file_data = {};

    ctrl.upload = uploadPhoto;
    ctrl.remove = remove;

    init();

    $scope.$on('validate', validate);

    function init() {
      $timeout(function() {
        if(ctrl.field && ctrl.field.value) { 
          ctrl.uploaderUrlImage = ctrl.field.value;
        }
        file_data = null;
      });

      $scope.$watch('ctrl.files', function(files) {
        files && uploadPhoto(files);
      });

      $scope.$watch('ctrl.field', function(nv, ov) {
        if(nv) {
          ctrl.field.name = nv.name;
          ctrl.uploaderUrlImage = nv.value;
        }
      });

      $scope.$watch('ctrl.field.value', function(newVal) {
        ctrl.uploaderUrlImage = newVal;
      });
    }

    function remove() {
      console.log(ctrl)
      if(!ctrl.disable) {
        ctrl.field.value = "";
        ctrl.uploaderUrlImage = null;
        ctrl.files = null
      }
    }

    function uploadPhoto(files) {
      if(files && files.name) {
        file = files;
        file_data = {};
        file_data[ctrl.field.name] = file;
        Upload.upload({
          url: ctrl.field.url,
          data: file_data,
          headers: {
            'x-session-key': ctrl.session
          }
        })
        .progress(function(evt) {
          ctrl.uploadProgress = parseInt(100.0 * evt.loaded / evt.total);
        })
        .success(function(data, status, headers, config) {
          ctrl.uploadProgress = 0;
          file = null;
          file_data = {};
          ctrl.field.value = data.result;
          ctrl.uploaderUrlImage = data.result;
          validate();
        });
      }
    }

    function validate() {
      if(!ctrl.uploaderUrlImage && ctrl.required) {
        ctrl.valid = false;
      } else {
        ctrl.valid = true;
      }
    }
  }
}
}());

;(function() {
"use strict";

var uniqueId = 1;

angular.module('idonate.core.inputs')
.directive('textField', TextField);

function TextField() {
  TextFieldCtrl.$inject = ["$scope", "iso4217"];
  return {
    bindToController: {
      blur: '=?',
      disable: '=?',
      field: '=?',
      label: '@?',
      mask: '@?',
      required: '=?',
      type: '@',
      valid: '=?',
      validator: '=?',
      placeholder: '=?',
      passwordMatch: '=?',
      passwordStrength:'=?',
      code: '=?',
    },
    compile: TextFieldCompile,
    controller: TextFieldCtrl,
    controllerAs: 'ctrl',
    restrict: 'E',
    scope: {},
    template:'<div ng-class=\"{\'invalid\':!ctrl.valid && ctrl.valid != null}\" class=form-group ng-switch on=ctrl.type><label for={{::ctrl.uniqueId}}>{{ctrl.label}}</label> <input name={{::ctrl.uniqueId}} ng-switch-when=text ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=text ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" ng-disabled=ctrl.disable> <input name={{::ctrl.uniqueId}} ng-if=ctrl.passwordMatch ng-switch-when=password ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=password ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" ng-disabled=ctrl.disable> <input name={{::ctrl.uniqueId}} ng-if=!ctrl.passwordMatch ng-switch-when=password ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=password ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" ng-disabled=ctrl.disable zxcvbn=ctrl.passwordStrength> <input name={{::ctrl.uniqueId}} ng-switch-when=email ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=email ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" ng-disabled=ctrl.disable> <input name={{::ctrl.uniqueId}} ng-switch-when=currency currency-symbol={{ctrl.curCode}} ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=text ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" ui-money-mask ng-disabled=ctrl.disable> <input name={{::ctrl.uniqueId}} ng-switch-when=alphanumeric ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=text ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" alphanumeric ng-disabled=ctrl.disable> <input name={{::ctrl.uniqueId}} ng-switch-when=number ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=number ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" ng-disabled=ctrl.disable> <input name={{::ctrl.uniqueId}} ng-switch-when=address ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=text ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" ng-disabled=ctrl.disable></div>'
  };

  function TextFieldCompile(element, attrs) {
    if(!attrs.type) {
      attrs.type = 'text';
    }
  }

  function TextFieldCtrl($scope, iso4217) {
    var ctrl = this;
    ctrl.uniqueId = 'textField' + uniqueId++;
    ctrl.onBlur = onBlur;
    ctrl.valid = null;

    $scope.$on('validate', validate);
    $scope.$watch('ctrl.code', function(newVal) {
      if(!newVal) { return ctrl.curCode = '$'; }
      ctrl.curCode = iso4217.getCurrencyByCode(newVal).symbol;
    });

    function onBlur() {
      if(ctrl.required) {
        validate();
      }

      if(ctrl.blur) {
        ctrl.blur();
      }
    }

    function validate() {
      if(!ctrl.field && ctrl.required) {
        ctrl.valid = false;
      }
      else {
        ctrl.valid = true;
      }
      if(ctrl.validator) {
        ctrl.valid = ctrl.validator(ctrl.field, ctrl.valid);
      }
    }
  }
}
}());

;(function() {
"use strict";

var uniqueId = 1;

angular.module('idonate.core.inputs')
.directive('textareaField', TextareaField);

function TextareaField() {
  TextareaFieldCtrl.$inject = ["$scope", "$timeout"];
  return {
    bindToController: {
      blur: '=?',
      disable: '=?',
      field: '=?',
      float: '=?',
      label: '@?',
      required: '=?',
      rows: '=?',
      valid: '=?',
      validator: '=?'
    },
    controller: TextareaFieldCtrl,
    controllerAs: 'ctrl',
    restrict: 'E',
    scope: {},
    template:'<div ng-class=\"{\'invalid\':!ctrl.valid && ctrl.valid != null}\" class=form-group><label for={{::ctrl.uniqueId}}>{{ctrl.label}}</label> <label class=pull-right>{{ctrl.field ? ctrl.field.length : \'0\'}} characters</label><summernote config=ctrl.options ng-disabled=ctrl.disable id={{::ctrl.uniqueId}} class=form-control ng-model=ctrl.field name={{ctrl.uniqueId}} on-init=ctrl.init() editor=editor></summernote></div>'
  };

  /*@ngInject*/
  function TextareaFieldCtrl($scope, $timeout) {
    var ctrl = this;
    ctrl.uniqueId = 'textareaField' + uniqueId++;
    ctrl.onBlur = onBlur;

    ctrl.valid = null;

    ctrl.init = function() {
      $timeout(function() {
        if (ctrl.disable){
          $scope.editor.summernote('disable');
        }
      });
    }

    $scope.$watch('ctrl.disable', function(newVal) {
      if(!newVal) {
        $timeout(function() {
          $scope.editor.summernote('enable');
        });
      }
    });

    var defaultOptions = {
      height: 300,
      focus: false,
      toolbar: [['style', ['bold', 'italic', 'underline']]],
      cleaner: {
        action:'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
        newline:'<p><br></p>', // Summernote's default is to use '<p><br></p>'
      },
      iconPrefix: '',
      icons: {
        'align': 'fa icon-align',
        'alignCenter': 'fa fa-align-center',
        'alignJustify': 'fa fa-align-justify',
        'alignLeft': 'fa fa-align-left',
        'alignRight': 'fa fa-align-right',
        'indent': 'fa fa-indent',
        'outdent': 'fa fa-outdent',
        'arrowsAlt': 'fa fa-arrows-alt',
        'bold': 'fa fa-bold',
        'caret': 'fa fa-caret-down',
        'circle': 'fa fa-circle',
        'close': 'fa fa-times',
        'code': 'fa fa-code',
        'eraser': 'fa fa-eraser',
        'font': 'fa fa-font',
        'frame': 'fa icon-frame',
        'italic': 'fa fa-italic',
        'link': 'fa icon-link',
        'unlink': 'fa fa-chain-broken',
        'magic': 'fa fa-magic',
        'menuCheck': 'fa fa-check',
        'minus': 'fa fa-minus',
        'orderedlist': 'fa fa-list-ol',
        'pencil': 'fa fa-pencil',
        'picture': 'fa fa-picture-o',
        'question': 'fa fa-question',
        'redo': 'fa fa-redo',
        'square': 'fa fa-square',
        'strikethrough': 'fa fa-strikethrough',
        'subscript': 'fa fa-subscript',
        'superscript': 'fa fa-superscript',
        'table': 'fa fa-table',
        'textHeight': 'fa fa-text-height',
        'trash': 'fa fa-trash',
        'underline': 'fa fa-underline',
        'undo': 'fa fa-undo',
        'unorderedlist': 'fa fa-ul',
        'video': 'fa fa-video'
      }
    };

    ctrl.options = defaultOptions;

    $scope.$on('validate', validate);

    function onBlur() {
      if(ctrl.required) {
        validate();
      }

      if(ctrl.blur) {
        ctrl.blur();
      }
    }

    function validate() {
      if(!ctrl.field && ctrl.required) {
        ctrl.valid = false;
      }
      else {
        ctrl.valid = true;
      }
      if(ctrl.validator) {
        ctrl.valid = ctrl.validator(ctrl.field, ctrl.valid);
      }
    }
  }
}
}());
