;(function() {
"use strict";

var uniqueId = 1;

angular.module('idonate.core.inputs')
.component('dateField', register());

function register() {
  return {
    template:'<div class=form-group><label for={{::$ctrl.uniqueId}} ng-if=\"$ctrl.label || $ctrl.description\">{{$ctrl.label}} <i ng-if=$ctrl.field.description tooltip={{$ctrl.field.description}} class=icon-question-mark></i></label><div class=input-group moment-picker=$ctrl.displayModel ng-model=$ctrl.ngModel format={{$ctrl.options.format}} start-view={{$ctrl.options.startView}} today=$ctrl.options.today min-view={{$ctrl.options.minView}} max-view={{$ctrl.options.maxView}} min-date=$ctrl.options.minDate max-date=$ctrl.options.maxDate disable=$ctrl.ngDisabled start-date=$ctrl.options.startDate><span class=input-group-addon><i class=icon-calendar></i></span> <input class=\"date-picker form-control\" placeholder=\"{{ $parent.$parent.$ctrl.placeholder }}\" ng-model=$ctrl.displayModel ng-class=\"{\'disabled\': $ctrl.ngDisabled}\" ng-disabled=$ctrl.ngDisabled disable=$ctrl.ngDisabled></div></div>',
    bindings: {
      required: '=?',
      valid: '=?',
      ngModel: '=',
      placeholder: '@',
      ngDisabled: '<',
      label: '@',
      userOptions: '<options',
      description: '@',
      ngBlur: '&'
    },
    controller: DateField
  };
}

function DateField($timeout, $scope) {
  var priv = {};
  this.uniqueId = 'dateField' + uniqueId++;
  priv.$timeout = $timeout;
  setDefaults.bind(this)();
  $scope.$watch('$ctrl.ngModel', blur.bind(this));
  this.$onChanges = $onChanges.bind(this);
  return this;

  function $onChanges(changesObj) {
    if(changesObj['userOptions']) {
      setDefaults.bind(this)();
    }
  }

  function setDefaults() {
    if(!this.options) {
      this.options = {};
    }
    angular.extend(this.options, {
      format: 'MM-DD-YYYY hh:mm A z',
      startView: 'month',
      today: 'true',
      minView: 'year',
      maxView: 'hour',
      minDate: moment(),
      startDate: moment(this.ngModel)
    }, this.userOptions);
  }

  function blur() {
    priv.$timeout((function() {
      this.ngBlur();
    }).bind(this));
  }
}
}());
