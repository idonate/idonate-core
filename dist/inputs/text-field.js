;(function() {
"use strict";

var uniqueId = 1;

angular.module('idonate.core.inputs')
.directive('textField', TextField);

function TextField() {
  return {
    bindToController: {
      blur: '=?',
      disable: '=?',
      field: '=?',
      label: '@?',
      mask: '@?',
      required: '=?',
      type: '@',
      valid: '=?',
      validator: '=?',
      placeholder: '=?',
      passwordMatch: '=?',
      passwordStrength:'=?',
      code: '=?',
    },
    compile: TextFieldCompile,
    controller: TextFieldCtrl,
    controllerAs: 'ctrl',
    restrict: 'E',
    scope: {},
    template:'<div ng-class=\"{\'invalid\':!ctrl.valid && ctrl.valid != null}\" class=form-group ng-switch on=ctrl.type><label for={{::ctrl.uniqueId}}>{{ctrl.label}}</label> <input name={{::ctrl.uniqueId}} ng-switch-when=text ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=text ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" ng-disabled=ctrl.disable> <input name={{::ctrl.uniqueId}} ng-if=ctrl.passwordMatch ng-switch-when=password ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=password ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" ng-disabled=ctrl.disable> <input name={{::ctrl.uniqueId}} ng-if=!ctrl.passwordMatch ng-switch-when=password ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=password ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" ng-disabled=ctrl.disable zxcvbn=ctrl.passwordStrength> <input name={{::ctrl.uniqueId}} ng-switch-when=email ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=email ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" ng-disabled=ctrl.disable> <input name={{::ctrl.uniqueId}} ng-switch-when=currency currency-symbol={{ctrl.curCode}} ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=text ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" ui-money-mask ng-disabled=ctrl.disable> <input name={{::ctrl.uniqueId}} ng-switch-when=alphanumeric ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=text ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" alphanumeric ng-disabled=ctrl.disable> <input name={{::ctrl.uniqueId}} ng-switch-when=number ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=number ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" ng-disabled=ctrl.disable> <input name={{::ctrl.uniqueId}} ng-switch-when=address ng-blur=ctrl.onBlur() id={{::ctrl.uniqueId}} class=form-control type=text ng-model=ctrl.field placeholder=\"{{ctrl.placeholder || ctrl.label}}\" ng-disabled=ctrl.disable></div>'
  };

  function TextFieldCompile(element, attrs) {
    if(!attrs.type) {
      attrs.type = 'text';
    }
  }

  function TextFieldCtrl($scope, iso4217) {
    var ctrl = this;
    ctrl.uniqueId = 'textField' + uniqueId++;
    ctrl.onBlur = onBlur;
    ctrl.valid = null;

    $scope.$on('validate', validate);
    $scope.$watch('ctrl.code', function(newVal) {
      if(!newVal) { return ctrl.curCode = '$'; }
      ctrl.curCode = iso4217.getCurrencyByCode(newVal).symbol;
    });

    function onBlur() {
      if(ctrl.required) {
        validate();
      }

      if(ctrl.blur) {
        ctrl.blur();
      }
    }

    function validate() {
      if(!ctrl.field && ctrl.required) {
        ctrl.valid = false;
      }
      else {
        ctrl.valid = true;
      }
      if(ctrl.validator) {
        ctrl.valid = ctrl.validator(ctrl.field, ctrl.valid);
      }
    }
  }
}
}());
