;(function() {
"use strict";

var dependencies = [
  'ui.select',
  'ngFileUpload',
  'moment-picker',
  'summernote',
  'zxcvbn',
  'isoCurrency',
];

angular.module('idonate.core.inputs', dependencies)
.config(Config);

function Config() {

}
}());
