;(function() {
"use strict";

var uniqueId = 1;

angular.module('idonate.core.inputs')
.directive('booleanField', BooleanField);

function BooleanField() {
  return {
    bindToController: {
      blur: '=?',
      disable: '=?',
      field: '=?',
      label: '@?',
      off: '@?',
      on: '@?',
      required: '=?',
      type: '@?',
      valid: '=?'
    },
    controller: BooleanFieldCtrl,
    controllerAs: 'ctrl',
    restrict: 'E',
    scope: {},
    template:'<div class=\"form-group checkbox-directive\"><label ng-if=ctrl.label>{{ctrl.label}}</label><div ng-switch on=ctrl.type class=checkbox-wrapper><div class=check-box-container ng-switch-when=blocky><label class=checkbox><input ng-disabled=ctrl.disable type=checkbox class=check-box-input ng-checked=\"ctrl.field == true\" ng-model=ctrl.field ng-change=ctrl.changed()> <span>{{ctrl.on}}</span> <span>{{ctrl.off}}</span></label></div><div class=onoffswitch ng-switch-when=switch><input type=checkbox name={{ctrl.uniqueId}} class=onoffswitch-checkbox id={{ctrl.uniqueId}} ng-disabled=ctrl.disable ng-checked=\"ctrl.field == true\" ng-model=ctrl.field ng-change=ctrl.changed()> <label class=onoffswitch-label for={{ctrl.uniqueId}}><span class=onoffswitch-inner></span> <span class=onoffswitch-switch></span></label></div></div></div>'
  };

  /*@ngInject*/
  function BooleanFieldCtrl($scope,$timeout) {
    var ctrl = this;

    init();

    $scope.$on('validate', validate);

    function init() {
      ctrl.uniqueId = 'booleanField' + uniqueId++
      if(ctrl.on == undefined) {
        ctrl.on = 'On';
      }
      if(ctrl.off == undefined) {
        ctrl.off = 'Off';
      }
      if(!ctrl.type) {
        ctrl.type = 'switch';
      }
    }

    function changed() {
      if(ctrl.blur) {
        $timeout(function() {
          ctrl.blur()
        });
      }
    }

    function validate() {
      if(ctrl.required && !ctrl.field) {
        ctrl.valid = false;
      } else {
        ctrl.valid = true;
      }
    }
  }
}
}());
