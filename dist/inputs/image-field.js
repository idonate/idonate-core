;(function() {
"use strict";

var uniqueId = 1;

angular.module('idonate.core.inputs')
.directive('imageField', ImageField);

function ImageField() {
  return {
    bindToController: {
      description: '=?',
      disable: '=?',
      field: '=',
      label: '@?',
      url: '=',
      required: '=?',
      session: '=',
      type: '@',
      valid: '=?'
    },
    controller: ImageFieldCtrl,
    controllerAs: 'ctrl',
    restrict: 'E',
    scope: {},
    template:'<div class=file-uploader-container ng-switch on=ctrl.type><label>{{ctrl.label}} <i ng-if=ctrl.description tooltip={{ctrl.description}} class=icon-question-mark></i></label><div ng-switch-when=buttons class=\"buttons-uploader row\"><div class=col-7 ng-show=!ctrl.uploaderUrlImage><div class=\"file-drop-area text-center\" ng-class=\"{\'file-present\':uploaderUrlImage || ctrl.files.name}\" ngf-drop ng-model=ctrl.files ngf-drag-over-class=dragover ngf-multiple=false ngf-accept=\"\'.jpg,.png\'\" ng-disabled=ctrl.disable>Drop your image here</div></div><div class=\"image-preview col-md-auto mb-1 mb-md-0 text-center-sm\" ng-show=\"ctrl.uploaderUrlImage || ctrl.files.name\"><div ng-show=\"!ctrl.uploaderUrlImage && ctrl.files.name\"><img class=img-fluid ngf-src=ctrl.files.value></div><div ng-show=\"ctrl.uploaderUrlImage && !ctrl.file\"><img class=img-fluid ng-src={{ctrl.uploaderUrlImage}}></div></div><div class=\"buttons-container col-md-auto\"><a class=\"btn btn-outline-primary d-block mb-1\" ng-show=!ctrl.disable ng-class=\"{\'file-present\':uploaderUrlImage || ctrl.files.name}\" ng-model=ctrl.files ngf-multiple=false ngf-select ngf-accept=\"\'.jpg,.png\'\" tabindex=0>Upload image</a> <a class=\"btn btn-outline-danger d-block\" ng-show=\"!ctrl.disable && (ctrl.uploaderUrlImage || ctrl.files.name)\" ng-click=ctrl.remove() ng-disabled=ctrl.disable tabindex=0>Remove image</a></div><div class=\"col-12 mt-2\" ng-show=\"ctrl.uploadProgress > 0\"><div class=progress><div class=progress-bar style=width:{{ctrl.uploadProgress}}%></div></div></div></div><div ng-switch-when=round class=\"round-uploader row\"><div class=file-drop-area ng-class=\"{\'file-present\':uploaderUrlImage || ctrl.files.name}\" ngf-select ngf-drop ng-model=ctrl.files ngf-drag-over-class=dragover ngf-selectngf-multiple=false ngf-accept=\"\'.jpg,.png\'\" ng-disabled=ctrl.disable><a ng-show=!ctrl.uploaderUrlImage>Upload image</a> <a ng-show=ctrl.uploaderUrlImage>Change image</a><div class=image-preview><div ng-show=\"!ctrl.uploaderUrlImage && ctrl.files.name\"><img class=img-fluid ngf-src=ctrl.files.value></div><div ng-show=\"ctrl.uploaderUrlImage && !ctrl.file\"><img class=img-fluid ng-src={{ctrl.uploaderUrlImage}}></div></div></div><div class=\"col-12 mt-2\" ng-show=\"ctrl.uploadProgress > 0\"><div class=progress style=\"height: 4.5px;\"><div class=progress-bar style=width:{{ctrl.uploadProgress}}%></div></div></div></div></div>'
  };

  /*@ngInject*/
  function ImageFieldCtrl($scope, $timeout, Upload) {
    var ctrl = this;
    var file = null;
    var file_data = {};

    ctrl.upload = uploadPhoto;
    ctrl.remove = remove;

    init();

    $scope.$on('validate', validate);

    function init() {
      $timeout(function() {
        if(ctrl.field && ctrl.field.value) { 
          ctrl.uploaderUrlImage = ctrl.field.value;
        }
        file_data = null;
      });

      $scope.$watch('ctrl.files', function(files) {
        files && uploadPhoto(files);
      });

      $scope.$watch('ctrl.field', function(nv, ov) {
        if(nv) {
          ctrl.field.name = nv.name;
          ctrl.uploaderUrlImage = nv.value;
        }
      });

      $scope.$watch('ctrl.field.value', function(newVal) {
        ctrl.uploaderUrlImage = newVal;
      });
    }

    function remove() {
      console.log(ctrl)
      if(!ctrl.disable) {
        ctrl.field.value = "";
        ctrl.uploaderUrlImage = null;
        ctrl.files = null
      }
    }

    function uploadPhoto(files) {
      if(files && files.name) {
        file = files;
        file_data = {};
        file_data[ctrl.field.name] = file;
        Upload.upload({
          url: ctrl.field.url,
          data: file_data,
          headers: {
            'x-session-key': ctrl.session
          }
        })
        .progress(function(evt) {
          ctrl.uploadProgress = parseInt(100.0 * evt.loaded / evt.total);
        })
        .success(function(data, status, headers, config) {
          ctrl.uploadProgress = 0;
          file = null;
          file_data = {};
          ctrl.field.value = data.result;
          ctrl.uploaderUrlImage = data.result;
          validate();
        });
      }
    }

    function validate() {
      if(!ctrl.uploaderUrlImage && ctrl.required) {
        ctrl.valid = false;
      } else {
        ctrl.valid = true;
      }
    }
  }
}
}());
