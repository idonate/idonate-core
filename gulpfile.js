const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const templates = require('angular-inject-templates');
var Server = require('karma').Server

const $ = gulpLoadPlugins();

gulp
.task('build', Build)
.task('clean', Clean)
.task('default', ['clean'], Default)
.task('test', ['build'], Test);


function Build() {
  return gulp.src(['src/**/*.config.js', 'src/**/*.js'])
  .pipe($.plumber())
  .pipe($.iife())
  .pipe(templates())
  .pipe(gulp.dest('dist'))
  .pipe($.ngAnnotate())
  .pipe($.concat('idonate-core.js'))
  .pipe(gulp.dest('dist'))
  .pipe($.uglify())
  .pipe($.rename('idonate-core.min.js'))
  .pipe(gulp.dest('dist'))
  .pipe($.size({title: 'build', gzip: true}));
}

function Clean() {
  return gulp.src('dist')
  .pipe($.clean());
}

function Default() {
  gulp.start('build');
}

function Test() {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, function() {}).start();
}
