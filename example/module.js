angular.module('idonate.core.example', ['ngRoute','idonate.core.inputs'])
.config(Config)
.controller('ExampleController', ExampleController);

function Config($routeProvider) {
  $routeProvider
  .when('/labels',{controller:'ExampleController',templateUrl:'/example/labels.html'})
  .otherwise({templateUrl: '/example/no-args.html'});
}

function ExampleController($scope,$timeout) {
  $scope.validate = validate;

  function validate() {
    console.log($scope.valid);
  }
}